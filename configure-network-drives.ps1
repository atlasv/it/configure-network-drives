$cred = Get-Credential -Message 'Enter Credentials'
$pw = $cred.GetNetworkCredential().Password
$user = $cred.GetNetworkCredential().Username

Get-Process explorer | Stop-Process
$net = new-object -ComObject WScript.Network
net use /delete V:
net use /delete W:
net use /delete X:
$net.MapNetworkDrive("V:", "\\192.168.1.250\AtlasV - Share", $false, "NAS-SAVE\$user", "$pw")
$net.MapNetworkDrive("W:", "\\192.168.1.250\AtlasV - Station", $false, "NAS-SAVE\$user", "$pw")
$net.MapNetworkDrive("X:", "\\192.168.1.250\AtlasV - Archives", $false, "NAS-SAVE\$user", "$pw")
start-process explorer
